/* 
    Task1: 

    Возьмите любую Free Rest Api, и с помощью объекта XMLHttpRequest выполните POST запрос согласно документации этого API. 

    Если есть проблема в поисках API, то можете воспользоваться этой : https://reqres.in/

    Читайте внимательно документацию самой API, дабы понять что должно отправляться при POST запросе.

    Outcome of task :

    Статус запроса: 200
    Ответ от API такой же как в ее документации

*/

const newPost = {
  title: "foo",
  body: "bar",
  userId: 1,
};

const addPost = (body, successResponse) => {
  const url = "https://jsonplaceholder.typicode.com/posts";
  const xhr = new XMLHttpRequest();

  xhr.open("POST", url);

  xhr.addEventListener("load", () => {
    successResponse(JSON.parse(xhr.response));
  });

  xhr.setRequestHeader("Content-type", "application/json; charset=UTF-8");

  xhr.send(JSON.stringify(body));
};

const successRequired = (response) => {
  console.log(response);
};

addPost(newPost, successRequired);

/* 
    Task2:

    Возьмите любую понравившуюся вам Free Rest API, и выполните Get запрос с помощью метода fetch()

    Если есть проблема в поисках API, то можете воспользоваться этой : https://reqres.in/

    Outcome of task :

    Статус запроса: 200
    Ответ от API такой же как в ее документации 

*/
const url2 = "https://jsonplaceholder.typicode.com/posts/1";

fetch(url2)
  .then((response) => response.json())
  .then((json) => console.log(json));

/* 
    Task3:
    
    Возьмите API с урока, с сайта jsonplaholder, и получите с сервера с помощью GET запроса комментарий с id 31. Пускай этот комментарий выводится в консоль по нажатию на кнопку которая будет у вас в html

*/

const btn = document.getElementById("btn");
const url3 = "https://jsonplaceholder.typicode.com/posts/";

const takeElem = (id) => fetch(url3 + id).then((elem) => elem.json());

btn.addEventListener("click", () => {
  takeElem(23)
    .then((answer) => console.log(answer))
    .catch((e) => console.log(e));
});

/* 
    Task4:
  
    Дан код:

    const getNumber = n => {
        return new Promise(r => {
            setTimeout(() => {
                r(n);
            },2000)
        })
    }

    const printNumber = n => {
        return new Promise(r => {
            getNumber(n).then(res => {
                r(res)
            })
        })
    }

    printNumber(100).then(data => console.log(data));


    Перепишите его используя операторы async/await
*/

const getNumber = (n) => {
  return new Promise((r) => {
    setTimeout(() => {
      r(n);
    }, 2000);
  });
};

async function printNumber(n) {
  return await getNumber(n);
}

printNumber(100).then((data) => console.log(data));

// const printNumber = n => {
//     return new Promise(r => {
//         getNumber(n).then(res => {
//             r(res)
//         })
//     })
// }

// printNumber(100).then(data => console.log(data));
