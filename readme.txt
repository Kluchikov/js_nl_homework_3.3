 /* 
    Task1: 

    Возьмите любую Free Rest Api, и с помощью объекта XMLHttpRequest выполните POST запрос согласно документации этого API. 

    Если есть проблема в поисках API, то можете воспользоваться этой : https://reqres.in/

    Читайте внимательно документацию самой API, дабы понять что должно отправляться при POST запросе.

    Outcome of task :

    Статус запроса: 200
    Ответ от API такой же как в ее документации

*/

/* 
    Task2:

    Возьмите любую понравившуюся вам Free Rest API, и выполните Get запрос с помощью метода fetch()

    Если есть проблема в поисках API, то можете воспользоваться этой : https://reqres.in/

    Outcome of task :

    Статус запроса: 200
    Ответ от API такой же как в ее документации

*/

/* 
    Task3:
    
    Возьмите API с урока, с сайта jsonplaholder, и получите с сервера с помощью GET запроса комментарий с id 31. Пускай этот комментарий выводится в консоль по нажатию на кнопку которая будет у вас в html

*/

/* 
    Task4:
  
    Дан код:

    const getNumber = n => {
        return new Promise(r => {
            setTimeout(() => {
                r(n);
            },2000)
        })
    }

    const printNumber = n => {
        return new Promise(r => {
            getNumber(n).then(res => {
                r(res)
            })
        })
    }

    printNumber(100).then(data => console.log(data));


    Перепишите его используя операторы async/await
*/
